package com.idgen.spring3.common.constants;

public class FieldNameConstants {

	public static final String FIRST_NAME = "firstName";
	
	public static final String LAST_NAME = "lastName";
	
}
