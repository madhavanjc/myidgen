package com.idgen.spring3.common.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CommonUtils {
	
	private CommonUtils(){
		
	}

	public static boolean isBlank(String str) {
		int strLen;
		if (str == null || (strLen = str.length()) == 0) {
			return true;
		}
		for (int i = 0; i < strLen; i++) {
			if ((Character.isWhitespace(str.charAt(i)) == false)) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean isBlankArray(String[] str) {
		if (str == null || (str.length == 0) || isBlank(str[0]) ) {
			return true;
		} else{
			return false;
		}
	}
	
	public static Map<String,String> getParamMap(Map<String,String[]> paramMapArray){
		Map<String,String> paramMap= new HashMap<String, String>();
		Set<String> keySet = paramMapArray.keySet();
		for(String key:keySet){
			if(!isBlankArray(paramMapArray.get(key))){
			paramMap.put(key,paramMapArray.get(key)[0]);
			}
		}
		return paramMap; 
	}
}
