package com.idgen.spring3.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.idgen.spring3.vo.LoggedInUser;

@Controller
public class BaseController {

	private LoggedInUser loggedInUser;
	
	
	@RequestMapping(value = "/hello")
	public String helloWorld(HttpSession session) {
		
		String message = "Hello World, Spring 3.0!";
		System.out.println(message);
		
//		mv.setViewName("enterDetails");
		
		
		return "enterDetails";
	}


	public LoggedInUser getLoggedInUser() {
		return loggedInUser;
	}


	public void setLoggedInUser(LoggedInUser loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

}
