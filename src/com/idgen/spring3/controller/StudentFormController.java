package com.idgen.spring3.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.idgen.spring3.service.impl.StudetnFromServiceImpl;
import com.idgen.spring3.service.interfaces.StudentFormServiceInt;
import com.idgen.spring3.vo.MasterDataDetails;
import com.idgen.spring3.vo.StudentAddressVO;
import com.idgen.spring3.vo.StudentInfo;

@Controller
public class StudentFormController  {

	private StudentFormServiceInt fromServiceInt; 
	
	public StudentFormController () {
		fromServiceInt = new StudetnFromServiceImpl();
	}
	
	@Autowired
	private MasterDataDetails masterDetails;
	
	@Autowired
	private StudentInfo studentInfo;
	
	@Autowired
	private StudentAddressVO studentAddressVO;
	
	ModelAndView mv ;
	

	@RequestMapping(value="/selectschool", method=RequestMethod.GET)
	public ModelAndView selectSchool(){
		ServletContext ctx = masterDetails.getServletContext();
		mv = new ModelAndView("selectschool");
		mv.addObject("schoolList", (List<String>)ctx.getAttribute("schoolList"));
		return mv;
	}
	
	@RequestMapping(value="/selectschool", method=RequestMethod.POST)
	public String selectSchoolRedirect(HttpServletRequest request,  HttpSession session){
		session.setAttribute("school", request.getParameter("school"));
		System.out.println("school selected:::  "+request.getParameter("school"));
		return "redirect:/enterDetails";
	}
	
	
	
	@RequestMapping(value = "/enterDetails", method = RequestMethod.GET)
	public ModelAndView loadForm(HttpSession session) {
		
		if(null==session.getAttribute("school")){
			System.out.println("entering the null mode*(*(_)*_)&(&)(7");
			mv = new ModelAndView("selectschool");
			mv.addObject("error", "Please select to continue");
			return mv;
		}
		mv = new ModelAndView("enterDetails");

		ServletContext ctx = masterDetails.getServletContext();
		
		if(null!= session.getAttribute("grade")) studentInfo.setGrade((String) session.getAttribute("grade"));
		if(null!= session.getAttribute("section")) studentInfo.setSection((String) session.getAttribute("section"));
		mv.addObject("student", studentInfo);
		mv.addObject("classesList", (List<String>)ctx.getAttribute("classesList"));
		mv.addObject("sectionList", (List<String>)ctx.getAttribute("sectionList"));
		mv.addObject("bloodGroupList", (List<String>)ctx.getAttribute("bloodGroupList"));
		
		return mv;
	}

	@RequestMapping(value = "/enterDetails", method = RequestMethod.POST)
	public ModelAndView saveForm(@ModelAttribute("student") StudentInfo info, HttpSession session) {
		session.setAttribute("grade", info.getGrade());
		session.setAttribute("section", info.getSection());
		mv = new ModelAndView("success");
		fromServiceInt.create(info); 
		return mv;
	}
	
	
	@RequestMapping(value="viewData")
	public ModelAndView viewStudentsData(){
		mv = new ModelAndView("viewData");
		List<StudentInfo> studentInfoList = new ArrayList<StudentInfo>();
		StudentInfo i = new StudentInfo(1, "_photoId", "_admissionNo", "_firstname", "_lastName", "_dob", "_gender", "_bloodGroup", "_grade", "_section", "school", studentAddressVO);
		studentInfoList.add(i);
		i = new StudentInfo(2, "_photoId2", "_admissionNo2", "_firstname2", "_lastName2", "_dob2", "_gender2", "_bloodGroup2", "_grade2", "_section2", "school2", studentAddressVO);
		studentInfoList.add(i);
		mv.addObject("studentList", studentInfoList);
		return mv;
	}
	
}
