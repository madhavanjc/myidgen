package com.idgen.spring3.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.idgen.spring3.vo.PreferenceVO;



public class PreferenceSelectController {

@Autowired
private PreferenceVO preferenceVO; 

	ModelAndView mv;
	
	@RequestMapping(value="preference", method= RequestMethod.GET)
	public ModelAndView choosePreference(@ModelAttribute("pref") PreferenceVO mymodel, BindingResult result){
		
		mv = new ModelAndView("preference");
		mv.addObject("sectionList", getSection());
		mv.addObject("classesList", getClasses());
		return mv;
	}
	
	
	@RequestMapping(value="preference", method= RequestMethod.POST)
	public String choosePreferencePost(@ModelAttribute("pref") PreferenceVO mymodel, BindingResult result){
		System.out.println(mymodel);
		return "redirect:enterDetails?clazz="+mymodel.getClasses()+"&section="+mymodel.getSection();
	}
	
	private List<String> getSection(){
		List<String> sections = new ArrayList<String>();
		sections.add("A");
		sections.add("B");
		sections.add("C");
		sections.add("D");
		return sections;
		
	}
	
	private List<String> getClasses(){
		List<String> classes = new ArrayList<String>();
		classes.add("I");
		classes.add("II");
		classes.add("III");
		classes.add("IV");
		classes.add("V");
		return classes ;
		
	}
}

