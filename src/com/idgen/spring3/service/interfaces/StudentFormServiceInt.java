package com.idgen.spring3.service.interfaces;

import com.idgen.spring3.vo.StudentInfo;

public interface StudentFormServiceInt {

	public void create(StudentInfo studentInfoVO);
}
