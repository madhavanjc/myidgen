package com.idgen.spring3.service.impl;

import com.idgen.spring3.dao.impl.StudentFormDaoImpl;
import com.idgen.spring3.dao.interfaces.StudentFormDaoInt;
import com.idgen.spring3.service.interfaces.StudentFormServiceInt;
import com.idgen.spring3.vo.StudentInfo;

public class StudetnFromServiceImpl implements StudentFormServiceInt {
	
	private StudentFormDaoInt daoInt;
	
	public StudetnFromServiceImpl(){
		daoInt = new StudentFormDaoImpl();
	}

	public void create(StudentInfo studentInfoVO) {
		daoInt.create(studentInfoVO);
	}
	

}
