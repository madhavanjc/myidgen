package com.idgen.spring3.dao.impl;

import com.idgen.spring3.dao.interfaces.StudentFormDaoInt;
import com.idgen.spring3.vo.StudentInfo;

public class StudentFormDaoImpl implements StudentFormDaoInt {
	
	public void create(StudentInfo studentInfoVO) {
		
		System.out.println("inside the student form dao implemention method"+ "\t" +"successful");
		System.out.println(studentInfoVO);
	}

}
