package com.idgen.spring3.dao.interfaces;

import com.idgen.spring3.vo.StudentInfo;

public interface StudentFormDaoInt {

	public void create(StudentInfo studentInfoVO);
}
