package com.idgen.spring3.vo;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;


public class StudentAddressVO {

	private String addressLine1;
	private String addressLine2;
	private String village;
	private String postal;
	private String taluk;
	private String district;
	private Integer pinCode;
	private String mobileNo;

	@PostConstruct
	private void init(){
		
	}
	
	@PreDestroy
	private void destroy(){
		
	}
	
	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public String getPostal() {
		return postal;
	}

	public void setPostal(String postal) {
		this.postal = postal;
	}

	public String getTaluk() {
		return taluk;
	}

	public void setTaluk(String taluk) {
		this.taluk = taluk;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public Integer getPinCode() {
		return pinCode;
	}

	public void setPinCode(Integer pinCode) {
		this.pinCode = pinCode;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	@Override
	public String toString() {
		return "StudentAddressVO [addressLine1=" + addressLine1
				+ ", addressLine2=" + addressLine2 + ", village=" + village
				+ ", mobileNo=" + mobileNo + "]";
	}
}
