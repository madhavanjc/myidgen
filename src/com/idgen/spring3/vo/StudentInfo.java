package com.idgen.spring3.vo;

public class StudentInfo {

	
	private int studentId;
	
	private String photoId;
	private byte[] image;
	private String admissionNo;
	
	private String firstName;
	private String lastName;
	private String dob;
	
	private String gender;
	private String bloodGroup;
	private String grade;
	
	private String section;
	private String school;
	private StudentAddressVO studentAddressVO;
	
	public StudentInfo(){
		
	}
	
	public StudentInfo(int _studentId, String _photoId, String _admissionNo, String _firstname, String _lastName,
			String _dob, String _gender, String _bloodGroup, String _grade, String _section, String _school, StudentAddressVO _studentAddressVO ){
		this.studentId = _studentId;
		this.photoId = _photoId;
		this.admissionNo = _admissionNo;
		this.firstName = _firstname;
		this.lastName = _lastName;
		this.dob = _dob;
		this.gender = _gender;
		this.bloodGroup = _bloodGroup;
		this.grade = _grade;
		this.section = _section;
		this.school = _school;
		this.studentAddressVO = _studentAddressVO;
	}
	
	
	
	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getPhotoId() {
		return photoId;
	}

	public void setPhotoId(String photoId) {
		this.photoId = photoId;
	}

	public String getAdmissionNo() {
		return admissionNo;
	}

	public void setAdmissionNo(String admissionNo) {
		this.admissionNo = admissionNo;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}


	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	
	@Override
	public String toString() {
		return "StudentInfo [admissionNo=" + admissionNo + ", firstName="
				+ firstName + ", lastName=" + lastName + ", dob=" + dob
				+ ", grade=" + grade + ", bloodGroup=" + bloodGroup + "StudentAddressVO=" + studentAddressVO + "]";
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public StudentAddressVO getStudentAddressVO() {
		return studentAddressVO;
	}

	public void setStudentAddressVO(StudentAddressVO studentAddressVO) {
		this.studentAddressVO = studentAddressVO;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}
  
}
