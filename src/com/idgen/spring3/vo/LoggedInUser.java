package com.idgen.spring3.vo;

import com.idgen.spring3.common.enums.UserTypeEnum;

public class LoggedInUser {

	private int userId;
	private String userName;
	private String displayName;
	private UserTypeEnum userType;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public UserTypeEnum getUserType() {
		return userType;
	}

	public void setUserType(UserTypeEnum userType) {
		this.userType = userType;
	}

	@Override
	public String toString() {
		return "LoggedInUser [userId=" + userId + ", userName=" + userName
				+ ", displayName=" + displayName + "]";
	}
	
	
}
