package com.idgen.spring3.vo;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.web.context.ServletContextAware;

public class MasterDataDetails implements ServletContextAware {

	private ServletContext servletContext;
	
	public void setServletContext(ServletContext servletContext) {
		this.servletContext= servletContext;
		servletContext.setAttribute("mynameis", "madhavan");
		servletContext.setAttribute("schoolList", getSchoolNames());
		servletContext.setAttribute("classesList", getClasses());
		servletContext.setAttribute("sectionList", getSections());
		servletContext.setAttribute("bloodGroupList", getBloodGroups());
	}

	public ServletContext getServletContext() {
		return servletContext;
	}
	
	
	private List<String> getSchoolNames(){
		List<String> schoolNames  = new ArrayList<String>();
		schoolNames.add("school 1 ");
		schoolNames.add("school 2 ");
		schoolNames.add("school 3 ");
		return schoolNames;
	}
	
	private List<String> getClasses(){
		List<String> classes  = new ArrayList<String>();
		classes.add("I"); classes.add("II");
		return classes;
	}
	
	private List<String> getSections(){
		List<String> sections  = new ArrayList<String>();
		sections.add("A");sections.add("B");
		return sections;
	}
	
	private List<String> getBloodGroups(){
		List<String> bloodGroups = new ArrayList<String>();
		bloodGroups.add("A positive"); bloodGroups.add("B positive");
		return bloodGroups;
	}
	
	
	
	
	
	

}
