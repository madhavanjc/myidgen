package com.idgen.spring3.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.idgen.spring3.dao.BaseDao;
import com.idgen.spring3.vo.MasterDataDetails;
import com.idgen.spring3.vo.StudentAddressVO;
import com.idgen.spring3.vo.StudentInfo;

@Configuration
public class UserConfig {

	@Bean
	public BaseDao baseDao(){
		BaseDao bean = new BaseDao();
		return bean;
	}
	
	@Bean
	public MasterDataDetails masterDetails(){
		MasterDataDetails bean = new MasterDataDetails();
		return bean;
	}
	
	@Bean
	@Scope("prototype")
	public StudentInfo studentInfo(){
		StudentInfo bean = new StudentInfo();
		return bean;
	}
	
	@Bean
	@Scope("prototype")
	public StudentAddressVO studentAddressVO(){
		StudentAddressVO bean = new StudentAddressVO();
		return bean;
	}
	
	
	
//	@Bean
	public DataSource dataSource(){
		DriverManagerDataSource bean = new DriverManagerDataSource();
		bean.setDriverClassName("");
		bean.setUrl("");
		bean.setUsername("");
		bean.setPassword("");
		return bean;
	}
	
}

