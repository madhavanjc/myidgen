package com.idgen.spring3.servlet;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ImageProcessor
 */
public class ImageProcessor extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public ImageProcessor() {
        super();
    
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String imageId = request.getParameter("photoid");
	//got the bytes from the blob from db
	System.out.println("servelet called for image processing");
	writeFile(readFile(), response);
	
	}
	
	private byte[] readFile(){
		try {
			File file = new File("c:\\image1.jpg");
			FileInputStream fs = new FileInputStream(file);
			byte[] mybyte = new byte[(int)file.length()];
			fs.read(mybyte);
			fs.close();
			return mybyte;
		} catch (Exception e) {
			System.out.println("exception in reading file");
			return null;
		}
	}
	
	private void writeFile(byte[] mybyte, HttpServletResponse response){
		try {
			response.reset(); 
			response.setContentType("image/jpg"); 
			response.getOutputStream().write(mybyte); 
			response.getOutputStream().flush(); 
		} catch (IOException e) {
			System.out.println("exception in writing");
			e.printStackTrace();
		}
	}

}
