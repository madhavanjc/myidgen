package com.idgen.spring3.authenticate;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;


public class SelfAuthentication implements AuthenticationProvider{

	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
		String uname = authentication.getName();
		String pass = authentication.getCredentials().toString();
		System.out.println("trying to login");
		System.out.println(authentication.getAuthorities());
		System.out.println(authentication.getPrincipal().toString());
		List<GrantedAuthority> grantedAuths = new ArrayList<GrantedAuthority>();
		grantedAuths.add(new SimpleGrantedAuthority("ROLE_USER") );
		return new UsernamePasswordAuthenticationToken(uname, pass, grantedAuths);
	}

	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
