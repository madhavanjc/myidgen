<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<script>
	function submitform(fm){
		fm.submit();
		
	}
</script>
<title>Student Details Form</title>
</head>
<%@ include file="header.jsp" %>
<body>
<form:form method="post" action="enterDetails" modelAttribute="student" id="studentform">	
	<fieldset>
  	<legend>Academics: </legend>
	<table>
	
		<tr>
			<td><form:label path="admissionNo">Admission No:</form:label>
			<td><form:input path="admissionNo"/>
		</tr>
	
		<tr>
			<td><form:label path="grade">Grade:</form:label></td>
			<td> <form:select path="grade">
					<form:options items="${classesList}"/>
				</form:select>
			<td><form:label path="section">Section</form:label></td>
			<td><form:select path="section">
					<form:options items="${sectionList}"/>
				</form:select></td>
		</tr>
		
	</table>
	</fieldset>
	
	<fieldset>
  	<legend>Personal Information:</legend>
	<table>		
		
		<tr>
			<td><form:label path="firstName">FirstName: </form:label></td>
			<td><form:input path="firstName" placeholder="Student's Name"/></td>
			<td><form:label path="lastName">lastName: </form:label></td>
			<td><form:input path="lastName" placeholder="Father's Name"/>
		</tr>
		
		<tr>
			<td><form:label path="dob">Date Of Birth: </form:label>
			<td><form:input path="dob" placeholder="dd/mm/yyyy"/>
		</tr>
		
		<tr>
			<td><form:label path="gender">Sex:</form:label></td>
			<td>Male: <form:radiobutton path="gender" value="M" checked="checked"/> Female:
			<form:radiobutton path="gender" value="F"/></td>
		</tr>
				
		<tr>
			<td><form:label path="bloodGroup">Blood Group</form:label></td>
			<td><form:select path="bloodGroup">
					<form:options items="${bloodGroupList}"/>
			</form:select></td>
		</tr>
		
	</table>
	</fieldset>
	
	
	<fieldset>
  	<legend>Address:</legend>
	<table>		
		<tr>
			<td><form:label path="studentAddressVO.addressLine1">Address Line 1: </form:label></td>
			<td><form:input path="studentAddressVO.addressLine1" placeholder="" cssStyle="width: 300px"/>
			<td><form:label path="studentAddressVO.addressLine2">Address Line 2: </form:label></td>
			<td><form:input path="studentAddressVO.addressLine2" placeholder="" cssStyle="width: 300px"/>
		</tr>
		
		<tr>
			<td><form:label path="studentAddressVO.village">Village: </form:label></td>
			<td><form:input path="studentAddressVO.village" placeholder="Village"/>
			<td><form:label path="studentAddressVO.postal">Postal: </form:label></td>
			<td><form:input path="studentAddressVO.postal" placeholder="Postal"/>
		</tr>
		
		<tr>
			<td><form:label path="studentAddressVO.taluk">Taluk: </form:label></td>
			<td><form:input path="studentAddressVO.taluk" placeholder="Taluk"/>
		</tr>
		
		<tr>
			<td><form:label path="studentAddressVO.district">District: </form:label></td>
			<td><form:input path="studentAddressVO.district" placeholder="District"/>
		</tr>
		
		<tr>
			<td><form:label path="studentAddressVO.pinCode">Pincode: </form:label></td>
			<td><form:input path="studentAddressVO.pinCode" placeholder="Pincode"/>
		</tr>
		
		<tr>
			<td><form:label path="studentAddressVO.mobileNo">Mobile Number: </form:label></td>
			<td><form:input path="studentAddressVO.mobileNo" placeholder="Parent's contact no."/>
		</tr>
		
		<tr>
			<td><input type="submit" value="click" onclick="submitform(this)"></td>
		</tr>
	</table>	
	</fieldset>
	</form:form>

	<div>
	${plist}
	</div>
	
	<h5>${student.firstName}</h5>
</body>
</html>