<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE >
<html>
<head>
<title>Insert title here</title>
</head>
<%@ include file="header.jsp" %>
<body>
	<span>
		<h3>Welcome.. Please select school to continue</h3>
	</span>

	<c:set var="error" scope="request" value="${error}" />
	<c:if test="${error !=null }">
		<p>
			<c:out value="${error}" />
		<p>
	</c:if>

	<form action="selectschool" method="POST">
		<select name="school">
			<c:forEach var='schoolList' items='${schoolList}'>
				<option value='<c:out value='${schoolList}'/>'><c:out
						value='${schoolList}' /></option>
			</c:forEach>
		</select> <input type="submit" value="set school">
	</form>

	<img alt="image1" height="400px" width="400px" src="<%=request.getContextPath()%>/getImage">

</body>
</html>